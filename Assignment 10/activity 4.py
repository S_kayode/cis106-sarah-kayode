#a program that uses the Nilakantha series to calculate Pi based on a given number
# of iterations entered by the user
#
#
#https://www.mathsisfun.com/numbers/pi.html
#https://en.wikiversity.org/wiki/Computer_Programming/Loops
#https://helloacm.com/two-simple-equations-to-compute-pi/



def get_number_of_iteration():
    print("Enter the number of iterations to calculate Pi:")
    number_of_iteration = int(input())
    return number_of_iteration
    
    
def displayResult(pi):
    print(pi)

    
def forLoop():
    pi = 3
    i= 2
    count = get_number_of_iteration()
    for i in range(1, count):
        if i % 2 == 1:
            pi += 4 / ((i*2)*(i*2+1)*(i*2+2))
            displayResult(pi)
        else:
            pi -= 4 / ((i*2)*(i*2+1)*(i*2+2))
            displayResult(pi)   
    return pi


#main
def main():
    pi = forLoop()
    displayResult(pi)
    
main()
