#a program that uses loop to generate a list of multiplication expressions for a given value
#
#References
#https://en.wikiversity.org/wiki/Computer_Programming/Loops


def get_expression():
    print("Enter the number of expressions to be displayed:")
    expression = int(input())
    return expression


def get_value():
    print("Enter the value:")
    value = int(input())
    return value


def calculate_multiplicationexpression(value, count):
    product = value * count
    return product


def displayResult(value, count, product):
    print(value, "*", count, "=", product)


def whileloop(expression, value):
    count = 1
    while count <= expression:
        product = value * count
        displayResult(value, count, product)
        count = count + 1
    return product


def forloop(expression, value):
    count = 1
    product = value * count
    for count in range(1, expression + 1, 1):
        product = value * count
        displayResult(value, count, product)
        count = count + 1
    return product


def main():
    value = get_value()
    expression = get_expression()
    whileloop(expression, value)
    forloop(expression, value)
    #displayResult(value, count, product)


main()
