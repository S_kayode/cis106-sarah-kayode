# This program calculates the gross pay
# 
#References
#https://en.wikiversity.org/wiki/Computer_Programming/Variables/Python3
#https://en.wikiversity.org/wiki/Computer_Programming/Functions/Flowchart


def calculate_overtime_grosspay(hours, rate):
    hours_overtime = hours - 40
    rate_overtime = 1.5 * rate
    overtime_grosspay = ((40 * rate) + (hours_overtime * rate_overtime))
    return overtime_grosspay


def calculate_grosspay(hours, rate):
    grosspay = hours * rate
    return grosspay
    

def displayResult(hours, rate, grosspay):
    print("Your grosspay is:", grosspay)


def get_hours():
    print("Enter hours:")
    hours = float(input())
    return hours


def get_rate():
    print("Enter rate:")
    rate = float(input())
    return rate


def main():
    hours = get_hours()
    rate = get_rate()
    if hours <= 40 :
        grosspay = calculate_grosspay(hours, rate)
        displayResult (hours, rate, grosspay)
    elif hours > 40:
        overtime_grosspay = calculate_overtime_grosspay(hours, rate)
        displayResult (hours, rate, overtime_grosspay)
    else:
        print("You must enter hours and rate to calculate gross pay!")

            
main()
