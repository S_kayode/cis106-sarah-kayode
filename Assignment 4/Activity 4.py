# This program calculates the approximate age in months, days, hours and seconds
# 
#References
#https://en.wikiversity.org/wiki/Computer_Programming/Variables/Python3

print("Enter Age:")
age = float(input())

months = age * 12
days = age * 365
hours = age * 365 * 24
seconds = age * 365 * 24 *60

print(months)
print(days)
print(hours)
print(seconds)
