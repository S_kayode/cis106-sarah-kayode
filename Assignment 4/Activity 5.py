# This program calculates and display distance in yards, feet and inches.
#
#References:
#https://en.wikiversity.org/wiki/Computer_Programming/Variables/Python3
#https://www.mathsisfun.com/measure/us-standard-length.html

print("Enter distance in miles:")
distance = float(input())

yard = distance * 1760
feet = distance * 1760 * 3
inches = distance * 1760 * 3 * 12

print(yard)
print(feet)
print(inches)
