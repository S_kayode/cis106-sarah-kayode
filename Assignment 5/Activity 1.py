This program calculates the gross pay
# 
#References
#https://en.wikiversity.org/wiki/Computer_Programming/Variables/Python3
#https://en.wikiversity.org/wiki/Computer_Programming/Functions/Flowchart


def calculategrosspay(hours, rate):
    grosspay = hours * rate
    
    return grosspay

def displayResult(hours, rate, grosspay):
    print(grosspay)

def gethours():
    print("Enter hours:")
    hours = float(input())
    
    return hours

def getrate():
    print("Enter rate:")
    rate = float(input())
    
    return rate

# Main


hours = gethours()
rate = getrate()
grosspay = calculategrosspay(hours, rate)
displayResult (hours, rate, grosspay)
