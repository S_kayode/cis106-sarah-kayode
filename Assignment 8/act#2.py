# This program calculates the approximate age in months, days, hours and seconds
# 
#References
#https://en.wikiversity.org/wiki/Computer_Programming/Variables/Python3


def calculate_months(age):
    months = age * 12
    return months


def calculate_days(age):
    days =  age * 365
    return days


def calculate_hours(age):
    hours = age * 365 * 24
    return hours


def calculate_seconds(age):
    seconds = age * 365 * 24 * 60 * 60
    return seconds
    

def displayresult(value, label):
    print(value, label)

    
def get_age():
    print("Enter age:")
    age = float(input())
    return age


def get_response():
    print("Would you will like to know your age in months, days, hours, or seconds?")
    response = input("Enter months, days, hours or seconds:")
    return response


def main():
    response = get_response()
    if response == "months":
        age = get_age()
        months = calculate_months(age)
        displayresult(months, "months")
    elif response == "days":
        age = get_age()
        days = calculate_days(age)
        displayresult(days, "days")
    elif response == "hours":
        age = get_age()
        hours = calculate_hours(age)
        displayresult(hours, "hours")     
    elif response == "seconds":
        age = get_age()
        seconds = calculate_seconds(age)
        displayresult(seconds, "seconds")
    else:
        print("Please enter your age and choose!")


main()

