# This program calculates and display distance in yards, feet and inches.
#
#References:
#https://en.wikiversity.org/wiki/Computer_Programming/Variables/Python3
#https://www.mathsisfun.com/measure/us-standard-length.html


def calculate_yard(distance):
    yard = distance * 1760
    return yard


def calculate_feet(distance):
    feet = distance * 1760 * 3
    return feet


def calculate_inches(distance):
    inches = distance * 1760 * 3 * 12
    return inches

    
def calculate_Kilometer(distance):
    Kilometer = distance * 1.60934
    return Kilometer


def calculate_meters(distance):
    meters = distance * 1.60934 * 1000
    return meters


def calculate_centimeters(distance):
    centimeters = distance * 1.60934 * 1000 * 100
    return centimeters


def displayresult(value, label):
    print(value, label)
    

def get_distance():
    print("Enter distance in miles:")
    distance = float(input())
    return distance


def get_response():
    print("Do you want the distance in US measurements or in metric measurements?")
    response = input("Enter US or metric:")
    return response


def main():
    response = get_response()
    if response == "US":
        distance = get_distance()
        yard = calculate_yard(distance)
        displayresult(yard, "yard")
        feet = calculate_feet(distance)
        displayresult(feet, "feet")
        inches = calculate_inches(distance)
        displayresult(inches, "inches")    
    elif response == "metric" :
        distance = get_distance()
        Kilometer = calculate_Kilometer(distance)
        displayresult(Kilometer, "Kilometers")
        meters = calculate_meters(distance)
        displayresult(meters, "meters")
        centimeters = calculate_centimeters(distance)
        displayresult(centimeters, "centimeters") 
    else:
        print("Please enter US or metric!")


main()    

