# This program asks the user to enter grade scores and calculates average
#  using While and for loops.
#
# References:
#     https://www.mathsisfun.com/definitions/average.html
#     https://en.wikiversity.org/wiki/Computer_Programming/Loops/Python3

def calculatetotalscores(score):
    totalscores = totalscores + score
    
    return totalscores

def displayResult(average, averageforloop):
    print("the average is:", average)
    print("the average for the forloop is:", averageforloop)

def forLoop():
    numberofscores = getnumberofscores()
    totalscores = 0
    for count in range(0, numberofscores + 0, 1):
        score = getscore()
        totalscores = totalscores + score
        averageforloop = totalscores / numberofscores
    
    return averageforloop

def getnumberofscores():
    print("Enter how many scores you will like to enter?")
    numberofscores = int(input())
    
    return numberofscores

def getscore():
    print("Enter score:")
    score = int(input())
    
    return score

def whileLoop():
    numberofscores = getnumberofscores()
    totalscores = 0
    count = 0
    while count < numberofscores:
        score = getscore()
        totalscores = totalscores + score
        count = count + 1
    average = totalscores / numberofscores
    
    return average

# Main
average = whileLoop()
averageforloop = forLoop()
displayResult (average, averageforloop)

