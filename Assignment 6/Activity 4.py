# This program calculate and displays the area of the shapes.
# 
#References
#https://en.wikiversity.org/wiki/Computer_Programming/Variables/Python3
#https://en.wikiversity.org/wiki/Computer_Programming/Functions/Flowchart
#https://www.mathsisfun.com/area.html


def calculate_triangle_area(base, height):
    area = 1/2 * base * height
    return area


def get_base():
    print("Enter the base:")
    base = float(input())
    return base


def calculate_rectangle_area(width, height):
    area = width * height
    return area


def get_width():
    print("Enter the width:")
    width = float(input())
    return width


def get_height():
    print("Enter the height:")
    height = float(input())
    return height



def calculate_trapezoid_area(lengtha, lengthb, height):
    area = 1/2 * (lengtha + lengthb) * height
    return area
    
def get_lengtha():
    print("Enter length a:")
    lengtha = float(input())
    return lengtha


def get_lengthb():
    print("Enter length b:")
    lengthb = float(input())
    return lengthb


def calculate_ellipse_area(a, b):
    area = a * b
    return area


def get_a():
    print("Enter a:")
    a = float(input())
    return a


def get_b():
    print("Enter b:")
    b = float(input())
    return b


def calculate_square_area(side):
    area = side * side
    return area


def get_length():
    print("Enter the length:")
    length = float(input())
    return length


def calculate_parallelogram_area(base, height):
    area = base * height
    return area


def calculate_circle_area(radius):
    area = 3.14 * radius * radius 
    return area


def get_radius():
    print("Enter the radius:")
    radius = float(input())
    return radius


def calculate_sector_area(radius, angle):
    area = 1/2 * radius * radius * angle
    return area
    
    
def get_angle():
    print("Enter the angle in radians:")
    angle = float(input())
    return angle


def display_result(triangle, rectangle, trapezoid, ellipse, square, parallelogram, circle, sector):
    print(triangle, "is the area of the triangle")
    print(rectangle, "is the area of the rectangle")
    print(trapezoid, "is thearea of the trapezoid")
    print(ellipse, "is thearea of the ellipse")
    print(square, "is the area of the square")
    print(parallelogram, "is the area of the parallelogram")
    print(circle, "is the area of the circle")
    print(sector, "is the area of the sector")


def main():
    base = get_base()
    height = get_height()
    triangle = calculate_triangle_area(base, height)
    
    width = get_width()
    height = get_height()
    rectangle = calculate_rectangle_area(width, height)
    
    lengtha = get_lengtha()
    lengthb = get_lengthb()
    height = get_height()
    trapezoid = calculate_trapezoid_area(lengtha, lengthb, height)

    a = get_a()
    b = get_b()
    ellipse = calculate_ellipse_area(a, b)
    
    length = get_length()
    square = calculate_square_area(length)
    
    base = get_base()
    height = get_height()
    parallelogram = calculate_parallelogram_area(base, height)
    
    radius = get_radius()
    circle = calculate_circle_area(radius)
    
    radius = get_radius()
    angle = get_angle()
    sector = calculate_sector_area(radius, angle)
    
    display_result(triangle, rectangle, trapezoid, ellipse, 
        square, parallelogram, circle, sector)
    
main()
