# This program calculates and display distance in yards, feet and inches.
#
#References:
#https://en.wikiversity.org/wiki/Computer_Programming/Variables/Python3
#https://www.mathsisfun.com/measure/us-standard-length.html


def get_distance():
    print("Enter distance in miles:")
    distance = float(input())
    return distance


def calculate_yard(distance):
    yard = distance * 1760
    return yard


def calculate_feet(distance):
    feet = distance * 1760 * 3
    return feet


def calculate_inches(distance):
    inches = distance * 1760 * 3 * 12
    return inches


def display_result(yard, feet, inches):
    print(yard, "yards")
    print(feet, "feet")
    print(inches, "inches")


def main():
    distance = get_distance()
    yard = calculate_yard(distance)
    feet = calculate_feet(distance)
    inches = calculate_inches(distance)
    display_result(yard, feet, inches)


main()
