# This program calculates the approximate age in months, days, hours and seconds
# 
#References
#https://en.wikiversity.org/wiki/Computer_Programming/Variables/Python3

def calculate_months(age):
    months = age * 12
    return months

def calculate_days(age):
    days =  age * 365
    return days

def calculate_hours(age):
    hours = age * 365 * 24
    return hours

def calculate_seconds(age):
    seconds = age * 365 * 24 * 60 * 60
    return seconds

def display_result(months, days, hours, seconds):
    print(months, "months")
    print(days, "days")
    print(hours, "hours")
    print(seconds, "seconds")

def get_age():
    print("Enter age:")
    age = float(input())
    return age

# Main
def main():
    age = get_age()
    months = calculate_months(age)
    days = calculate_days(age)
    hours = calculate_hours(age)
    seconds = calculate_seconds(age)
    display_result (months, days, hours, seconds)

main()