# This program demonstrates the order of operations
a = 5
b = 6
c = 7
d = 2
print("a = ", + a)
print("b = ", + b)
print("c = ", + c)
print("d =", + d)
print("a + b =", + (a + b))
print("c - b = ", + (c - b))
print("a + b + c = ", + (a + b + c))
print("d * a + b - c = ", + (d * a + b - c))
print("d * a + b - c / c = ", + (d * a + b - float(c) / c))
print("d * a ^ d + b - c = ", + (d * a ** d + b - c))
print(" (d * a ^ d + b) - c = ", + (d * a ** d + b - c))
